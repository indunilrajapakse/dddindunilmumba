const SL_AWS = require('slappforge-sdk-aws');
const connectionManager = require('./ConnectionManager');
const rds = new SL_AWS.RDS(connectionManager);

exports.handler = async (event) => {

    // You must always end/destroy the DB connection after it's used
    rds.beginTransaction({
        instanceIdentifier: 'induid'
    }, function (error, connection) {
        if (error) {
            throw error;
        }
    });

    return { "message": "Successfully executed" };
};