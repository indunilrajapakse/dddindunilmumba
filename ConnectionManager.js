module.exports = function() {
    this.dbConnections = [];

    this.dbConnections["induid"] = {
        host: process.env.EndPoint_rdsInduid,
        port: process.env.Port_rdsInduid,
        user: process.env.User_rdsInduid,
        password: process.env.Password_rdsInduid,
        database: "indudname"
    };
};